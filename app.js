let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
require('./database')

const usersRouter = require('./api/users.route');
const sessionsRouter = require('./api/sessions.route');
const devicesRouter = require('./api/devices.route');
let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/api/v1',
    usersRouter,
    sessionsRouter,
    devicesRouter
);

app.use(function(err, req, res, next) {
    console.log(err);
    if (err.type === "entity.parse.failed" && err.statusCode === 400){
      return res.json({
        success: false,
        error: "Invalid json"
      });
    };
    res.status(500).json({
      success: false,
      error: {
          message: 'Internal server error'
      }
      });
  })
  
module.exports = app;
