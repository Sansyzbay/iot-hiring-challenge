// Update with your config settings.
const path = require('path')
require('dotenv').config({path: path.resolve('.env')});

function normalizePoolSize(val) {
  var pool = parseInt(val, 10);

  if (isNaN(pool)) {
    // named pipe
    return val;
  }

  if (pool >= 0) {
    // port number
    return pool;
  }

  return false;
}

module.exports = {

  development: {
    client: process.env.DEV_DB_CLIENT,
    connection: {
      database: process.env.DEV_DB,
      user:     process.env.DEV_DB_USER,
      password: process.env.DEV_DB_PASSWORD,
      host: process.env.DEV_DB_HOST,
      port: process.env.DEV_DB_PORT
    },
    pool: {
      min: normalizePoolSize(process.env.DEV_DB_POOL_MIN) || 2,
      max: normalizePoolSize(process.env.DEV_DB_POOL_MAX) || 10
    }
  },

  staging: {
    client: process.env.DEV_DB_STAGING,
    connection: {
      database: process.env.STAGING_DB,
      user:     process.env.STAGING_DB_USER,
      password: process.env.STAGING_DB_PASSWORD,
      host: process.env.DEV_DB_HOST,
      port: process.env.STAGING_DB_PORT
    },
    pool: {
      min: normalizePoolSize(process.env.STAGING_DB_POOL_MIN) || 2,
      max: normalizePoolSize(process.env.STAGING_DB_POOL_MAX) || 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: process.env.PRODUCTION_DB_STAGING,
    connection: {
      database: process.env.PRODUCTION_DB,
      user:     process.env.PRODUCTION_DB_USER,
      password: process.env.PRODUCTION_DB_PASSWORD,
      host: process.env.PRODUCTION_DB_HOST,
      port: process.env.PRODUCTION_DB_PORT
    },
    pool: {
      min: normalizePoolSize(process.env.PRODUCTION_DB_POOL_MIN) || 2,
      max: normalizePoolSize(process.env.PRODUCTION_DB_POOL_MAX) || 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};



