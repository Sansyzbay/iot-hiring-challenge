'use strict'

const { Model } = require('objection');


class UserDevice extends Model {
    static get tableName(){
        return 'user_devices'
    }

    $beforeUpdate(){
        this.updated_at = new Date().toISOString();
    }

}


module.exports = UserDevice;