'use strict'

const { Model } = require('objection');

 class Session extends Model {
     static get tableName(){
         return 'sessions';
     }

     $beforeUpdate(){
         this.updated_at = new Date().toISOString();
     }
 }


 module.exports = Session