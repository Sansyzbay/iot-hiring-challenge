'use strict'

const { Model } = require('objection');

class User extends Model {
    static get tableName(){
        return 'users';
    }

    $beforeUpdate(){
        this.updated_at = new Date().toISOString();
    }

    static get relationMappings(){
        return {
            devices: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/UserDevice',
                join: {
                    from: 'users.id',
                    to: 'user_devices.user_id'
                }
            },
            session: {
                relation: Model.HasManyRelation,
                modelClass: __dirname + '/Session',
                join: {
                    from: 'users.id',
                    to: 'sessions.user_id'
                }
            }
        }
    }
}


module.exports = User;