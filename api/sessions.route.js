const express = require('express');

const router = express.Router();
const { SessionsController } = require('./sessions.controller');

const oauth = require('../_helpers/authentication');

router.route('/session')
    .post(SessionsController.apiPostSession)
    .delete(oauth, SessionsController.apiDeleteSession)


module.exports = router;