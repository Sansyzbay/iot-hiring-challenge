const express = require('express');

const router = express.Router();
const { UsersController } = require('./users.controller');
const oauth = require('../_helpers/authentication');

router.route('/users')
    .post(UsersController.apiPostUser)

router.route('/users/:userId')
    .get(oauth, UsersController.apiGetUser)

module.exports = router;