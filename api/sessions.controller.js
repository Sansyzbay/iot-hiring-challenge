const Joi = require('@hapi/joi')
const UsersDAO = require('../dao/usersDAO');
const SessionsDAO = require('../dao/sessionsDAO');
const {User} = require('./users.controller')
class SessionsController {
    static async apiPostSession(req, res, next){
        try {
            const userCredentialsFromBody = {...req.body}
            const userCredentialsValidation = Joi.object().keys({
                email: Joi.string().email({minDomainSegments: 2}).lowercase().required(),
                password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/).required(),
            }).validate(userCredentialsFromBody);

            if (userCredentialsValidation.error){
                return res.status(400).json({error: "Invalid credentials format"})
            }

            const userCredentials = userCredentialsValidation.value;
            
            const findUserResult = await UsersDAO.findUser({email: userCredentials.email});
            if(findUserResult.error){
              throw new Error(findUserResult.error);
            }

            if(!findUserResult.value){
            return res.status(404).json({
                error: {
                    message: "User not found"
                }
              });
            }

            const user = new User(findUserResult.value);

            console

            const isValidPassword = await user.comparePassword(userCredentials.password);
            if(!isValidPassword){
                return res.status(401).json({
                    error: {
                        message: "Invalid password"
                    }
                });
            }

            //generate token
            const authToken = user.encoded();

            //create session
            const userAgent = req.get('user-agent');
            const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

            const session = {
                user_id: user.id,
                agent: userAgent,
                ip_address: ip,
                token: authToken,
            }
            
            const createSessionResult = await SessionsDAO.createSession(session);

            if (createSessionResult.error){
                throw new Error(createSessionResult.error);
            }

            res.status(200).json({ 
              auth_token: authToken, 
              user: user.toJson() 
            });
        } catch (error) {
            next(error);
        }
    }

    static async apiDeleteSession(req, res, next){
        try {
            const findSessionResult = await SessionsDAO
                .findSession({
                    token: req.token, 
                    user_id: req.user.id,
                    id_blocked: false
                });
            
            if (findSessionResult.error){
                throw new Error(findSessionResult.error)
            }

            if (!findSessionResult.value){
                return res.status(404).json({error: "Not found session"})
            }

            const softDeleteSessionResult = await SessionsDAO.softDeleteSessionById(findSessionResult.value.id);

            if (softDeleteSessionResult.error){
                throw new Error(softDeleteSessionResult.error)
            }

            res.status(200).json({
                success: true
            })

        } catch (error) {
            next(error)
        }
    }
}


module.exports = {
    SessionsController
}