const Joi = require('@hapi/joi');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const UsersDAO = require('../dao/usersDAO');
const hashPassword = async password => bcrypt.hash(password, 10);


class User {
    constructor({
        id, 
        first_name, 
        last_name,
        email,
        role,
        phone,
        password,
        gender,
        created_at,
        updated_at
     }){
         this.id = id;
         this.first_name = first_name;
         this.last_name = last_name;
         this.email = email;
         this.role = role;
         this.phone = phone;
         this.password = password;
         this.gender = gender;
         this.created_at = created_at;
         this.updated_at = updated_at;
     }

     toJson(){
         return {
             id: this.id,
             first_name: this.first_name,
             last_name: this.last_name,
             email: this.email,
             role: this.role,
             phone: this.phone,
             gender: this.gender,
             created_at: this.created_at,
             updated_at: this.updated_at
         }
     }

     async comparePassword(plainText) {
         return await bcrypt.compare(plainText, this.password);
     }

     static async decoded(userJwt){
         return new Promise(function(resolve, reject){
             jwt.verify(userJwt, process.env.SECRET_KEY, (error, res) => {
                 if ( error ){
                     return reject(error);
                 }
                 resolve(res);
             });
         });
     }

     encoded(){
         return jwt.sign({
             exp: Math.floor(Date.now() / 1000)  + 3600,
             ...this.toJson(),
         },
         process.env.SECRET_KEY)
     }
}

class UsersController{

    static async apiPostUser(req, res, next){
        try{
            const userFromBody = {...req.body};
            const userValidation = Joi.object().keys({
                email: Joi.string().email().lowercase().required(),
                password: Joi.string().regex(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/).required(),
                first_name: Joi.string().min(1).max(255).required(),
                last_name: Joi.string().min(1).max(255).required(),
                phone: Joi.string().min(5).max(13).allow('', null),
                gender: Joi.string().valid('male', 'female').allow(null),
            }).validate(userFromBody);

            if ( userValidation.error){
                return res.status(400).json({error: "Invalid user"});
            }

            const  findUserResult = await UsersDAO.findUser({email: userValidation.value.email});

            if (findUserResult.error){
                //TODO: handle database error;
                throw new Error(findUserResult.error);
            }

            if (findUserResult.value){
                return res.status(400).json({error: "User already exists"});
            }
        
            let user = new User({
                ...userValidation.value,
                password: await hashPassword(userValidation.value.password)
            });

            const createUserResult = await UsersDAO.createUser(user);

            if (createUserResult.error){
                //TODO: handle database error
                throw new Error(createUserResult.error)
            }

            res.status(200).json({user: createUserResult.value})
        }catch (error){
            next(error);
        }
    }


    static async apiGetUser(req, res, next){
        try {
            const userId = req.params.userId;

            const findUserResult = await UsersDAO.findUser({id: userId});

            if (findUserResult.error){
                throw new Error(findUserResult.error);
            }

            if (!findUserResult.value){
                return res.status(404).json({error: "Not found user"})
            }

            const user = new User(findUserResult.value)

            res.status(200).json({
                user: user.toJson()
            })
        } catch (error) {
            next(error);
        }
    }
}

module.exports = {
    User,
    UsersController
}