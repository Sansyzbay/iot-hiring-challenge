const express = require('express');
const router = express.Router();
const  {DevicesController} = require('./devices.controller')
const oauth = require('../_helpers/authentication');

router.route('/devices')
    .post(oauth, DevicesController.apiPostDevice)

router.route('/devices/:deviceId')
    .get(oauth, DevicesController.apiGetDevice)
    .patch(oauth, DevicesController.apiPatchDevice)
    .delete(oauth, DevicesController.apiDeleteDevice)


module.exports = router;