const DevicesDAO = require('../dao/devicesDAO');
const Joi = require('@hapi/joi');


class DevicesController{
    static async apiGetDevice(req, res, next){
        try {
            const deviceId = req.params.deviceId;
            const findDeviceResult = await DevicesDAO.findDevice({id: deviceId, user_id: req.user.id});

            if (findDeviceResult.error){
                throw new Error(findDeviceResult.error);
            }

            if (!findDeviceResult.value){
                return res.status(404).json({error: "Not found device"})
            }

            res.status(200).json({
                device: findDeviceResult.value
            });
        } catch (error) {
            next(error)
        }
    }

    static async apiPostDevice(req, res, next){
        try {
            const deviceFromBody = req.body;

            const deviceValidation = Joi.object().keys({
                title: Joi.string().min(1).max(255).required(),
                description: Joi.string().min(1).max(500).allow('',null),
                value: Joi.string().min(1).max(255).allow('', null),
                unit: Joi.string().min(1).max(255).allow('', null),
            }).validate(deviceFromBody)

            if (deviceValidation.error){
                return res.status(400).json({error: "Invlaid device"})
            }

            const device = {
                ...deviceValidation.value,
                user_id: req.user.id
            }

            const createDeviceResult = await DevicesDAO.createDevice(device);

            if (createDeviceResult.error){
                throw new Error(createDeviceResult.error);
            }

            res.status(200).json({
                device: createDeviceResult.value
            });
        } catch (error) {
            next(error);
        }
    }


    static async apiPatchDevice(req, res, next){
        try {
            const deviceId = req.params.deviceId;
            const deviceFromBody = req.body;
            const deviceValidation = Joi.object().keys({
                title: Joi.string().min(1).max(255),
                description: Joi.string().min(1).max(500).allow('',null),
                value: Joi.string().min(1).max(255).allow('', null),
                unit: Joi.string().min(1).max(255).allow('', null),
            }).validate(deviceFromBody);

            if(deviceValidation.error){
                return res.status(400).json({error: "Invalid device"});
            }

            const findDeviceResult = await DevicesDAO.findDevice({id: deviceId, user_id: req.user.id})
            if (findDeviceResult.error){
                throw new Error(findDeviceResult.error);
            }

            if (!findDeviceResult.value){
                return res.status(404).json({error: "Not found device"})
            }

            const updateDeviceResult = await DevicesDAO.updateDeviceById(deviceId, deviceValidation.value);

            if (updateDeviceResult.error){
                throw new Error(updateDeviceResult.error);
            }

            res.status(200).json({
                device: updateDeviceResult.value
            });
        } catch (error) {
            next(error);
        }
    }


    static async apiDeleteDevice(req, res, next){
        try {
            const deviceId = req.params.deviceId;
            
            const findDeviceResult = await DevicesDAO.findDevice({id: deviceId, user_id: req.user.id})
            if (findDeviceResult.error){
                throw new Error(findDeviceResult.error);
            }

            const deleteDeviceResult = await DevicesDAO.deleteDeviceById(deviceId);

            if (deleteDeviceResult.error){
                throw new Error(deleteDeviceResult.error);
            }
            res.status(200).json({success: true})
        } catch (error) {
            next(error);
        }
    }
}

module.exports = {
    DevicesController
}