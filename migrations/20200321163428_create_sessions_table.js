
exports.up = function(knex) {
  return knex.schema.createTable('sessions', table => {
    table.increments('id').primary();
    table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('CASCADE');
    table.string('agent');
    table.string('ip_address');
    table.text('token');
    table
        .boolean('is_blocked')
        .defaultTo(false)
    table
        .timestamp('created_at',{ useTz: true })
        .notNullable()
        .defaultTo(knex.fn.now());
    table
        .timestamp('updated_at',{ useTz: true })
        .notNullable()
        .defaultTo(knex.fn.now());
  });
};

exports.down = function(knex) {
    return knex.schema.raw('DROP TABLE sessions CASCADE');
};
