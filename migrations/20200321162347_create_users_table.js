
exports.up = function(knex) {
  return knex.schema.createTable('users', table => {
    table.increments('id').primary();
    table.string('first_name');
    table.string('last_name');
    table
        .string('phone')
    table
        .string('email')
        .unique()
        .notNullable();
    table.text('password');
    table.string('gender');
    table.string('role');

    table
        .timestamp('created_at', {useTz: true})
        .notNullable()
        .defaultTo(knex.fn.now());
    
    table
        .timestamp('updated_at', {useTz: true})
        .notNullable()
        .defaultTo(knex.fn.now());
      });
};

exports.down = function(knex) {
  return knex.schema.raw('DROP TABLE users CASCADE');
};
