
exports.up = function(knex) {
  return knex.schema.createTable('user_devices', table => {
    table
        .increments('id')
        .primary();
    table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .onDelete('SET NULL');
    table.string('title');
    table.string('description', 500);
    table.string('value');
    table.string('unit');
    table
        .timestamp('updated_at',{ useTz: true })
        .notNullable()
        .defaultTo(knex.fn.now());
    
    table
        .timestamp('created_at',{ useTz: true})
        .notNullable()
        .defaultTo(knex.fn.now());
  });
};

exports.down = function(knex) {
  return knex.schema.raw('DROP TABLE user_devices CASCADE;')
};
