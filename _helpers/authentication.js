const { User } = require('../api/users.controller');

module.exports = async function(req, res, next){
    if (!req.get('Authorization')) {
        return res.status(403).json({
          success: false,
          errors: [
            {
            message: 'InvalidToken'
          } 
        ]
        });
      }
      const userJwt = req.get('Authorization').slice('Bearer '.length);
      let userObj
      try {
        userObj = await User.decoded(userJwt);
      } catch (error) {
        return res.status(401).json({ 
          success: false, 
          errors: "InvalidToken"
        });
      }
      if(!req.user) req.user = userObj;
      if(!req.token) req.token = userJwt;
      next();
}