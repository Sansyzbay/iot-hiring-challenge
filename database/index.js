const { Pool } = require('pg');
const { Model } = require('objection');
const Knex = require('knex');
const knexConfig = require('../knexfile');
let pool;
let knex;
if ( process.env.NODE_ENV === "production"){
  pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
  });
  knex = Knex(knexConfig.production);
}else{
  pool = new Pool({
    user: process.env.DEV_DB_USER,
    host: process.env.DEV_DB_HOST,
    database: process.env.DEV_DB_DATABASE,
    password: process.env.DEV_DB_PASSWORD,
    port: process.env.DEV_DB_PORT,
  });
  knex = Knex(knexConfig.development);
}
  
Model.knex(knex);

module.exports = {
  query: (text, params, callback) => pool.query(text, params, callback),
  pool: pool
};  
