const Session = require('../models/Session');


class SessionsDAO {
    static async createSession(session){
        try {
            const sessionFromDatabase = await Session
                .query()
                .insert(session)
                .returning('*')
            return {value: sessionFromDatabase}
        } catch (error) {
            return {error}
        }
    }

    static async findSession(wFilter){
        try {
            const sessionFromDatabase = await Session
                .query()
                .findOne(wFilter);
            return {value: sessionFromDatabase}
        } catch (error) {
            return {error}
        }
    }

    static async updateSessionById(id, session){
        try {
            const sessionFromDatabase = await Session
                .query()
                .patchAndFetchById(id, session);
            
            return {value: sessionFromDatabase}
        } catch (error) {
            return {error}
        }
    }


    static async softDeleteSessionById(id){
        try {
            const deletedSession = await Session
                .query()
                .patchAndFetchById(id, {is_blocked: true});
            return {value: deletedSession}
        } catch (error) {
            return {error}
        }
    }


}

module.exports = SessionsDAO;