const User = require('../models/User');

class UsersDAO {
    static async findUser({id, email}){
        try {
            let userFromDatabase;
            if (id){
                userFromDatabase = await User
                    .query()
                    .findById(id);
            }else if(email){
                userFromDatabase = await User
                    .query()
                    .findOne({email});
            }else {
                throw new Error('Specify id or email address');
            }
            return {
                value: userFromDatabase
            }
        } catch (error) {
            return { error }
        }
    }

    static async createUser(user){
        try {
            const userFromDatabase = await User
                .query()
                .insert(user)
                .returning('*');
            return {
                value: userFromDatabase
            }
        }catch (error){
            return {error}
        }
    }


    static async updateUserById(id, user){
        try {
            const numberOfUpdatedUsers = await User
                .query()
                .patchAndFetchById(id,user);
            return {
                value: numberOfUpdatedUsers
            }
        } catch (error) {
            return {error}
        }
    }


    static async deleteUserById(id){
        try {
            const numberOfDeletedUsers = await User
                .query()
                .findById(id)
                .delete()
            return {
                value: numberOfDeletedUsers
            }
        } catch (error) {
            return {error}
        }
    }
}


module.exports = UsersDAO;