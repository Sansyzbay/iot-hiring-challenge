const UserDevice = require('../models/UserDevice');

class DevicesDAO {

    static async findDevice(wFilter){
        try {
           const deviceFromDatabase = await UserDevice
                .query()
                .select('title','description','value', 'unit')
                .findOne(wFilter);
            
            return { value: deviceFromDatabase}
        } catch (error) {
            return {error}
        }
    }

    static async createDevice(device){
        try {
            const deviceFromDatabase = await UserDevice
                .query()
                .insert(device)
                .returning('*');
            
            return {value: deviceFromDatabase}
        } catch (error) {
            return {error}  
        }
    }

    static async updateDeviceById(id, newDevice){
        try {
            const deviceFromDatabase = await UserDevice
                .query()
                .patchAndFetchById(id, newDevice);
            
            return { value: deviceFromDatabase }
        } catch (error) {
            return {error}
        }
    }


    static async deleteDeviceById(id){
        try {
            const numberOfDeleted = await UserDevice
                .query()
                .findById(id)
                .delete();

            return { value: numberOfDeleted}
        } catch (error) {
            return {error}
        }
    }

}


module.exports = DevicesDAO;